<?php

namespace Courier\Adapters;

use Courier\Request\CreateShipmentRequest;
use Courier\Request\TrackShipmentRequest;
use Courier\Response\CreateShipmentResponse;
use Courier\Response\TrackShipmentResponse;
use Courier\Couriers\CourierNumberOne\Request\CreateShipmentAndGetWaybillRequest;
use Courier\Couriers\CourierNumberOne\Request\ShipmentTrackingDetailsRequest;
use Courier\Couriers\CourierNumberOne\Response\CreateShipmentAndGetWaybillResponse;
use Courier\Couriers\CourierNumberOne\Response\ShipmentTrackingDetailsResponse;
use Courier\CourierInterface;
use Courier\Couriers\CourierNumberOne\CourierNumberOneInterface;

class CourierNumberOneAdapter implements CourierInterface {

    private $CourierNumberOne;

    public function __construct(CourierNumberOneInterface $CourierNumberOne) {
        $this->CourierNumberOne = $CourierNumberOne;
    }

    public function createShipment(CreateShipmentRequest $createShipmentRequest): CreateShipmentResponse {
        $CreateShipmentAndGetWaybillRequest = $this->createShipmentAndGetWaybillRequest($createShipmentRequest);
        $createShipmentAndGetWaybillReponse = $this->CourierNumberOne->createShipmentAndGetWaybill($CreateShipmentAndGetWaybillRequest);
        $createShipmentResponse = $this->getCreateShipmentResponse($createShipmentAndGetWaybillReponse);
        return $createShipmentResponse;
    }

    public function trackShipment(TrackShipmentRequest $trackShipmentRequest): TrackShipmentResponse {
        $shipmentTrackingDetailsRequest = $this->getShipmentTrackingDetailsRequest($trackShipmentRequest);
        $shipmentTrackingDetailsResponse = $this->CourierNumberOne->getShipmentTrackingDetails($shipmentTrackingDetailsRequest);
        $trackShipment = $this->getTrackShipmentResponse($shipmentTrackingDetailsResponse);
        return $trackShipment;
    }

    private function getCreateShipmentResponse(CreateShipmentAndGetWaybillResponse $createShipmentAndGetWaybillReponse): CreateShipmentResponse {
        $createShipmentResponse = new CreateShipmentResponse();
        $createShipmentResponse->setShipmentID((string) $createShipmentAndGetWaybillReponse->getShipmentID());
        return $createShipmentResponse;
    }

    private function createShipmentAndGetWaybillRequest(CreateShipmentRequest $createShipmentRequest): CreateShipmentAndGetWaybillRequest {
        $createShipmentAndGetWaybillRequest = new CreateShipmentAndGetWaybillRequest();
        $createShipmentAndGetWaybillRequest->SetReceiverName((string) $createShipmentRequest->getReceiverName());
        $createShipmentAndGetWaybillRequest->setProductName((string) $createShipmentRequest->getProductName());
        $createShipmentAndGetWaybillRequest->setSenderFirstName((string) $createShipmentRequest->getSenderName());
        return $createShipmentAndGetWaybillRequest;
    }

    private function getTrackShipmentResponse(ShipmentTrackingDetailsResponse $shipmentTrackingDetailsResponse): TrackShipmentResponse {
        $trackShipment = new TrackShipmentResponse();
        $trackShipment->setLocation((string) $shipmentTrackingDetailsResponse->getLocation());
        return $trackShipment;
    }

    private function getShipmentTrackingDetailsRequest(TrackShipmentRequest $trackShipmentRequest): ShipmentTrackingDetailsRequest {
        $shipmentTrackingDetailsRequest = new ShipmentTrackingDetailsRequest();
        $shipmentTrackingDetailsRequest->setShipmentID((string) $trackShipmentRequest->getID());
        return $shipmentTrackingDetailsRequest;
    }

}
