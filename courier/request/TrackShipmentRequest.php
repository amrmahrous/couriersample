<?php

namespace Courier\Request;

class TrackShipmentRequest implements TrackShipmentRequestInterface {

    private $ID;

    public function setID(string $id) {
        $this->ID = $id;
        return $this;
    }

    public function getID() {
        return $this->ID;
    }

}
