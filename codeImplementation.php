<?php

include_once 'autoload.php';

use Courier\Request\CreateShipmentRequest;
use Courier\Request\TrackShipmentRequest;
use Courier\Factory\CourierFactory;
use Courier\CourierEnum;

// Create Shipment Request data ;
$createShipmentRequest = new CreateShipmentRequest();
$createShipmentRequest->setSenderName('Amr Mahrous');
$createShipmentRequest->setReceiverName('Ahmed Habashy');
$createShipmentRequest->setProductName('Product X');

$courierFactory = new CourierFactory();
$courierClient = $courierFactory->getCourrierObject(CourierEnum::CourierNumberTwo);
$createShipmentResponse = $courierClient->createShipment($createShipmentRequest);

var_dump($createShipmentResponse);
// track Shipment Request data
$trackShipmentRequest = new TrackShipmentRequest();
$trackShipmentRequest->setID = 'IDX568DHL-EG-Tanta';

$trackShipmentResponse = $courierClient->trackShipment($trackShipmentRequest);
var_dump($trackShipmentResponse);
