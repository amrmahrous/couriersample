<?php

namespace Courier\Couriers\CourierNumberOne\Request;

interface ShipmentTrackingDetailsRequestInterface {

    public function setShipmentID(string $id);

    public function getShipmentID();
}
