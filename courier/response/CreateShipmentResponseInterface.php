<?php

namespace Courier\Response;

interface CreateShipmentResponseInterface {

    public function setShipmentID(string $id);

    public function getShipmentID();
}
