<?php

namespace Courier\Couriers\CourierNumberTwo;

interface CourierNumberTwoInterface {

    public function createShipmentInfo();

    public function createShipmentInstructions();

    public function getShipmentWaybill();

    public function getShipmentTrackingDetails();
}
