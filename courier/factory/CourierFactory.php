<?php

namespace Courier\Factory;

use Courier\CourierEnum;
use Courier\CourierInterface;
use Courier\Adapters\CourierNumberOneAdapter;
use Courier\Adapters\CourierNumberTwoAdapter;
use Courier\Adapters\CourierNumberThreeAdapter;
use Courier\Couriers\CourierNumberOne\CourierNumberOne;
use Courier\Couriers\CourierNumberTwo\CourierNumberTwo;
use Courier\Couriers\CourierNumberThree\CourierNumberThree;

class CourierFactory implements CourierFactoryInterface {

    public function getCourrierObject($CourierEnum): CourierInterface {
        switch ($CourierEnum) {
            case CourierEnum::CourierNumberOne :
                $courierNumberOne = new CourierNumberOne();
                return new CourierNumberOneAdapter($courierNumberOne);
            case CourierEnum::CourierNumberTwo :
                $courierNumberTwo = new CourierNumberTwo();
                return new CourierNumberTwoAdapter($courierNumberTwo);
            case CourierEnum::CourierNumberThree :
                $courierNumberThree = new CourierNumberThree();
                return new CourierNumberThreeAdapter($courierNumberThree);
            default :
                throw new \Exception('Courier Not supporter');
        }
    }

}
