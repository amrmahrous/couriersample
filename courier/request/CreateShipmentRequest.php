<?php

namespace Courier\Request;

class CreateShipmentRequest implements CreateShipmentRequestInterface {

    private $SenderName, $ReceiverName, $ProductName;

    public function setSenderName(string $senderName) {
        return $this->SenderName = $senderName;
    }

    public function SetReceiverName(string $receiverName) {
        return $this->ReceiverName = $receiverName;
    }

    public function setProductName(string $productName) {
        return $this->ProductName = $productName;
    }

    public function getSenderName() {
        return $this->SenderName;
    }

    public function getReceiverName() {
        return $this->ReceiverName;
    }

    public function getProductName() {
        return $this->ProductName;
    }

}
