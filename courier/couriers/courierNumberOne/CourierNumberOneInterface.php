<?php

namespace Courier\Couriers\CourierNumberOne;

use Courier\Couriers\CourierNumberOne\Request\CreateShipmentAndGetWaybillRequest;
use Courier\Couriers\CourierNumberOne\Request\ShipmentTrackingDetailsRequest;
use Courier\Couriers\CourierNumberOne\Response\CreateShipmentAndGetWaybillResponse;
use Courier\Couriers\CourierNumberOne\Response\ShipmentTrackingDetailsResponse;

interface CourierNumberOneInterface {

    public function createShipmentAndGetWaybill(CreateShipmentAndGetWaybillRequest $createShipmentData): CreateShipmentAndGetWaybillResponse;

    public function getShipmentTrackingDetails(ShipmentTrackingDetailsRequest $ShipmentTrackingDetailsRequest): ShipmentTrackingDetailsResponse;
}
