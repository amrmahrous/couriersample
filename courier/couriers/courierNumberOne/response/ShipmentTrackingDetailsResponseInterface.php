<?php

namespace Courier\Couriers\CourierNumberOne\Response;

interface ShipmentTrackingDetailsResponseInterface {

    public function setLocation(string $location);

    public function getLocation();
}
