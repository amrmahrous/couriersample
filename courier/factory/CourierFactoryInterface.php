<?php

namespace Courier\Factory;

use Courier\CourierEnum;
use Courier\CourierInterface;

interface CourierFactoryInterface {

    public function getCourrierObject($CourierEnum): CourierInterface;
}
