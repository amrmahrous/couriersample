<?php

namespace Courier\Couriers\CourierNumberOne\Response;

interface CreateShipmentAndGetWaybillResponseInterface {

    public function setShipmentID(string $id);

    public function getShipmentID();
}
