<?php

namespace Courier\Request;

interface CreateShipmentRequestInterface {

    public function setSenderName(string $senderName);

    public function setReceiverName(string $receiverName);

    public function setProductName(string $productName);

    public function getSenderName();

    public function getReceiverName();

    public function getProductName();
}
