<?php

namespace Courier\Response;

interface TrackShipmentResponseInterface {

    public function setLocation(string $location);

    public function getLocation();
}
