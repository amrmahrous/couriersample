<?php

namespace Courier\Couriers\CourierNumberOne\Response;

class CreateShipmentAndGetWaybillResponse implements CreateShipmentAndGetWaybillResponseInterface {

    private $ShipmentID;

    public function setShipmentID(string $id) {
        $this->ShipmentID = $id;
        return $this;
    }

    public function getShipmentID() {
        return $this->ShipmentID;
    }

}
