<?php

namespace Courier\Response;

class TrackShipmentResponse implements TrackShipmentResponseInterface {

    private $location;

    public function setLocation(string $location) {
        $this->location = $location;
        return $this;
    }

    public function getLocation() {
        return $this->location;
    }

}
