<?php

namespace Courier\Couriers\CourierNumberOne\Request;

class ShipmentTrackingDetailsRequest implements ShipmentTrackingDetailsRequestInterface {

    private $ShipmentID;

    public function setShipmentID(string $id) {
        $this->ShipmentID = $id;
        return $this;
    }

    public function getShipmentID() {
        return $this->ShipmentID;
    }

}
