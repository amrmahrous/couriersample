<?php

namespace Courier\Request;

interface TrackShipmentRequestInterface {

    public function setID(string $id);

    public function getID();
}
