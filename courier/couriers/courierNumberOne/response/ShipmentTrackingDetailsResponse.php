<?php

namespace Courier\Couriers\CourierNumberOne\Response;

class ShipmentTrackingDetailsResponse implements ShipmentTrackingDetailsResponseInterface {

    private $location;

    public function setLocation(string $location) {
        $this->location = $location;
        return $this;
    }

    public function getLocation() {
        return $this->location;
    }

}
