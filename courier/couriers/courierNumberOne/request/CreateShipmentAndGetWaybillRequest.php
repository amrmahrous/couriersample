<?php

namespace Courier\Couriers\CourierNumberOne\Request;

class CreateShipmentAndGetWaybillRequest implements CreateShipmentAndGetWaybillRequestInterface {

    private $SenderFirstName, $SenderLastName, $ReceiverName, $ProdName;

    public function setSenderFirstName(string $senderFirstName) {
        return $this->SenderFirstName = $senderFirstName;
    }

    public function setSenderLastName(string $senderLastName) {
        return $this->SenderLastNameName = $senderLastName;
    }

    public function SetReceiverName(string $receiverName) {
        return $this->ReceiverName = $receiverName;
    }

    public function setProductName(string $productName) {
        return $this->ProductName = $productName;
    }

    public function getSenderFirstName() {
        return $this->SenderFirstName;
    }

    public function getSenderLastName() {
        return $this->SenderLastName;
    }

    public function getReceiverName() {
        return $this->ReceiverName;
    }

    public function getProductName() {
        return $this->ProductName;
    }

}
