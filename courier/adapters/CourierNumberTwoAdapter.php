<?php

namespace Courier\Adapters;
use Courier\Request\CreateShipmentRequest;
use Courier\Request\TrackShipmentRequest;
use Courier\Response\CreateShipmentResponse;
use Courier\Response\TrackShipmentResponse;
use Courier\Couriers\CourierNumberTwo\CourierNumberTwoInterface;
use Courier\CourierInterface;

class CourierNumberTwoAdapter implements CourierInterface {

    private $CourierNumberTwo;

    public function __construct(CourierNumberTwoInterface $CourierNumberTwo) {
        $this->CourierNumberTwo = $CourierNumberTwo;
    }

    public function createShipment(CreateShipmentRequest $createShipmentRequest) : CreateShipmentResponse {
        $createShipmentInfoReponse = $this->CourierNumberTwo->createShipmentInfo();
        $createShipmentInstructionsReponse = $this->CourierNumberTwo->createShipmentInstructions();
        $getShipmentWaybillReponse = $this->CourierNumberTwo->getShipmentWaybill();
        return new CreateShipmentResponse();
    }

    public function trackShipment(TrackShipmentRequest $trackShipmentRequest) : TrackShipmentResponse {
        $getShipmentTrackingDetailsResponse = $this->CourierNumberTwo->getShipmentTrackingDetails();
        return new TrackShipmentResponse();
    }

}
