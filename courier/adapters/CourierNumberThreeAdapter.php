<?php

namespace Courier\Adapters;
use Courier\Request\CreateShipmentRequest;
use Courier\Request\TrackShipmentRequest;
use Courier\Response\CreateShipmentResponse;
use Courier\Response\TrackShipmentResponse;
use Courier\Couriers\CourierNumberThree\CourierNumberThreeInterface;
use Courier\CourierInterface;

class CourierNumberThreeAdapter implements CourierInterface {

    private $CourierNumberThree;

    public function __construct(CourierNumberThreeInterface $CourierNumberThree) {
        $this->CourierNumberThree = $CourierNumberThree;
    }

    public function createShipment(CreateShipmentRequest $createShipmentRequest) : CreateShipmentResponse {
        $createShipmentAndGetWaybillReponse = $this->CourierNumberThree->createShipmentAndGetWaybill();
        return new CreateShipmentResponse();
    }

    public function trackShipment(TrackShipmentRequest $trackShipmentRequest) : TrackShipmentResponse {
        $registerNumberToGetTrackingDetailsResponse = $this->CourierNumberThree->registerNumberToGetTrackingDetails();
        $getTrackingDetailsResponse = $this->CourierNumberThree->getTrackingDetails();
        return new TrackShipmentResponse();
    }

}
