<?php

namespace Courier;

use Courier\Response\CreateShipmentResponse;
use Courier\Response\TrackShipmentResponse;
use Courier\Request\CreateShipmentRequest;
use Courier\Request\TrackShipmentRequest;

interface CourierInterface {

    public function createShipment(CreateShipmentRequest $request): CreateShipmentResponse;

    public function trackShipment(TrackShipmentRequest $request): TrackShipmentResponse;
}
