<?php

namespace Courier\Couriers\CourierNumberOne\Request;

interface CreateShipmentAndGetWaybillRequestInterface {

    public function setSenderFirstName(string $senderfirstName);

    public function setSenderlastName(string $senderlastName);

    public function setReceiverName(string $receiverName);

    public function setProductName(string $productName);

    public function getSenderFirstName();

    public function getReceiverName();

    public function getProductName();
}
