<?php

namespace Courier\Couriers\CourierNumberThree;

interface CourierNumberThreeInterface {

    public function createShipmentAndGetWaybill();

    public function registerNumberToGetTrackingDetails();

    public function getTrackingDetails();
}
