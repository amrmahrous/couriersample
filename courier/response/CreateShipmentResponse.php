<?php

namespace Courier\Response;

class CreateShipmentResponse implements CreateShipmentResponseInterface {

    private $ShipmentID;

    public function setShipmentID(string $id) {
        $this->ShipmentID = $id;
        return $this;
    }

    public function getShipmentID() {
        return $this->ShipmentID;
    }

}
